#include <iostream>
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <opencv2/highgui/highgui.hpp>

enum {
	USE_KNN = 1,
	KNN_COUNT = 2,

	MIN_MATCHES = 10,

	CAM_WIDTH = 640,
	CAM_HEIGHT = 480,
};

typedef std::vector<cv::DMatch> match_vec;

static void filter_matches(const match_vec &in, match_vec &out, float min_dist)
{
	for (match_vec::const_iterator it = in.begin(); it != in.end(); it++) {
		const cv::DMatch &match = *it;
		if (match.distance < fmax(3 * min_dist, 0.02f)) {
			out.push_back(match);
		}
	}
}

static void drawFrame(const cv::Mat &marker, const cv::Mat &scene,
	const cv::Mat &homography, cv::Mat out)
{
	std::vector<cv::Point2f> marker_corners(4), scene_corners(4);
	marker_corners[0] = cv::Point(0, 0);
	marker_corners[1] = cv::Point(marker.cols, 0);
	marker_corners[2] = cv::Point(marker.cols, marker.rows);
	marker_corners[3] = cv::Point(0, marker.rows);

	cv::perspectiveTransform(marker_corners, scene_corners, homography);
	cv::Point2f offset(marker.cols, 0);

	for (int i = 0; i < 4; i++) {
		line(out, marker_corners[i], marker_corners[(i + 1) % 4],
			cv::Scalar(255, 0, 0), 4);

		line(out, scene_corners[i] + offset,
			scene_corners[(i + 1) % 4] + offset,
			cv::Scalar(0, 255, 0), 4);
	}
}

static void match(const cv::DescriptorMatcher &matcher,
	const cv::Mat &dsc_marker, const cv::Mat &dsc_scene,
	match_vec &out)
{
	match_vec matches;
	matcher.match(dsc_marker, dsc_scene, matches);

	float min_dist = 999.0f;
	for (match_vec::iterator it = matches.begin(); it != matches.end(); it++) {
		const cv::DMatch &match = *it;
		min_dist = std::min(min_dist, match.distance);
	}
	std::cout << "Min distance " << min_dist << std::endl;

	filter_matches(matches, out, min_dist);
}

static void matchKnn(const cv::DescriptorMatcher &matcher,
	const cv::Mat &dsc_marker, const cv::Mat &dsc_scene,
	match_vec &out)
{
	std::vector<match_vec> mvec;
	matcher.knnMatch(dsc_marker, dsc_scene, mvec, KNN_COUNT);
	std::cout << "KNN matches: " << mvec.size() << std::endl;
	for (size_t i = 0; i < mvec.size(); i++) {
		if (mvec[i][0].distance > 0.8 * (mvec[i][1].distance)) {
			continue;
		}
		if (mvec[i].size() != KNN_COUNT) {
			std::cout << mvec[i].size() << "\n";
			continue;
		}
		out.push_back(mvec[i][0]);
	}
}

static void showMatches(const cv::Mat &img_marker, const cv::Mat &img_scene,
	cv::Mat &img_matches)
{
	using namespace cv;
	OrbFeatureDetector detector;
	std::vector<KeyPoint> kp_marker, kp_scene;

	detector.detect(img_marker, kp_marker);
	detector.detect(img_scene, kp_scene);

	//FREAK extractor;
	ORB extractor;
	Mat dsc_marker, dsc_scene;
	extractor.compute(img_marker, kp_marker, dsc_marker);
	extractor.compute(img_scene, kp_scene, dsc_scene);

	BFMatcher matcher(NORM_HAMMING);
	match_vec good_matches;

	if (USE_KNN) {
		matchKnn(matcher, dsc_marker, dsc_scene, good_matches);
	}
	else {
		match(matcher, dsc_marker, dsc_scene, good_matches);
	}

	std::cout << "Matched " << good_matches.size() << " keypoints" << std::endl;

	drawMatches(img_marker, kp_marker, img_scene, kp_scene,
		good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
		vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

	std::vector<Point2f> pt_marker, pt_scene;
	for (match_vec::iterator it = good_matches.begin();
		it != good_matches.end(); it++)
	{
		cv::DMatch &match = *it;
		pt_marker.push_back(kp_marker[match.queryIdx].pt);
		pt_scene.push_back(kp_scene[match.trainIdx].pt);
	}

	if (min(pt_marker.size(), pt_scene.size()) > min(4, (int)MIN_MATCHES)) {
		Mat H = findHomography(pt_marker, pt_scene, LMEDS);
		drawFrame(img_marker, img_scene, H, img_matches);
	}
}

static void usage(const char *appname);

static int detector(const char *path_marker, const char* path_scene)
{

	cv::Mat img_marker = cv::imread(path_marker, cv::IMREAD_GRAYSCALE);
	cv::Mat img_scene = cv::imread(path_scene, cv::IMREAD_GRAYSCALE);

	if ((NULL == img_marker.data) || (NULL == img_scene.data)) {
		std::cerr << "Failed to load images" << std::endl;
		return -1;
	}

	cv::Mat img_matches;
	showMatches(img_marker, img_scene, img_matches);
	cv::imshow("Good matches", img_matches);
	cv::waitKey(0);

	return 0;
}

int main_camera(int argc, char **argv)
{
	int have_marker = 0;
	cv::Mat img_marker, img_frame, img_matches;

	if (argc == 2)
	{
		img_marker = cv::imread(argv[1], cv::IMREAD_GRAYSCALE);
		if (img_marker.empty()) {
			std::cerr << "failed to load the marker image" << std::endl;
			return -1;
		}
		cv::resize(img_marker, img_marker, cv::Size(CAM_WIDTH, CAM_HEIGHT));
		have_marker = 1;
	}

	cv::VideoCapture capture(0);
	capture.set(CV_CAP_PROP_FRAME_WIDTH, CAM_WIDTH);
	capture.set(CV_CAP_PROP_FRAME_HEIGHT, CAM_HEIGHT);
	
	if (!capture.isOpened()) {
		std::cout << "No camera detected" << std::endl;
		return -1;
	}

	do {
		if (!capture.grab()) {
			perror("could not grab frame");
			continue;
		}

		cv::Mat tmp;
		capture.retrieve(tmp);

		if (!have_marker) {
			if (tmp.empty()) {
				std::cout << "empty marker" << std::endl;
				continue;
			}
			cv::cvtColor(tmp, img_marker, CV_RGB2GRAY);
			if (!img_marker.empty()) {
				have_marker = 1;
			}
			continue;
		}
		else {
			if (tmp.empty()) {
				std::cout << "empty frame" << std::endl;
				continue;
			}
			cv::cvtColor(tmp, img_frame, CV_RGB2GRAY);
			if (img_frame.empty()) {
				continue;
			}
		}

		if (img_marker.size() != img_frame.size()) {
			std::cout << "m" << img_marker.size() << "\n";
			std::cout << "f" << img_frame.size() << "\n";
			continue;
		}

		showMatches(img_marker, img_frame, img_matches);
		cv::imshow("preview", img_matches);
	} while (cvWaitKey(1) != 'q');

	return 0;
}

int main_files(int argc, char **argv)
{
	if (3 != argc) {
		usage(argv[0]);
		return -1;
	}
	return detector(argv[1], argv[2]);
}

static void usage(const char *appname) {
	std::cerr << "Usage: " << appname << " input_marker input_scene"
		<< std::endl;
	std::cerr << "Capture camera: " << appname << " input_marker" << std::endl;
}

int main(int argc, char **argv)
{
	if (3 == argc) {
		return main_files(argc, argv);
	}
	else {
		return main_camera(argc, argv);
	}
}
