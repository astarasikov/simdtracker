#!/bin/bash

function push_lib {
	adb push "opencv/platforms/build_android_arm/lib/armeabi-v7a/$1" /sdcard/
	adb shell "su -c cp /sdcard/$1 /data/data/org.opencv.engine/lib"
	adb shell "su -c chmod 755 /data/data/org.opencv.engine/lib/$1"
	adb shell "su -c chown system:system /data/data/org.opencv.engine/lib/$1"
	adb shell "su -c rm /sdcard/$1"
}

push_lib "libopencv_java.so"
