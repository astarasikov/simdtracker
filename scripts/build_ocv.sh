#!/bin/bash

export ANDROID_NDK=/Users/alexander/Documents/workspace/bin/android/android-ndk-r9d
export ANDROID_ABI="armeabi-v7a with NEON"
export ANDROID_NDK_TOOLCHAIN_NAME="arm-linux-androideabi-4.8"

pushd .
cd ./opencv/platforms/
sh ./scripts/cmake_android_arm.sh
cd build_android_arm
make -j8
popd
