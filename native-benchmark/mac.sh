#!/bin/bash

export NDK_PATH="${NDK_PATH:-/Users/alexander/Documents/workspace/bin/android/android-ndk-r9d}"
export ANDROID_ROOT="${NDK_PATH}/platforms/android-14/arch-arm/usr"


if [[ $COMPILER == "clang" ]]; then
	export CC="${NDK_PATH}/toolchains/llvm-3.2/prebuilt/darwin-x86_64/bin/clang"
	export PATH="${NDK_PATH}/toolchains/llvm-3.2/prebuilt/darwin-x86_64/bin:$PATH"
else
	export PATH="${NDK_PATH}/toolchains/arm-linux-androideabi-4.8/prebuilt/darwin-x86_64/bin:$PATH"
	export CROSS_COMPILE=arm-linux-androideabi-
	export CC="${CROSS_COMPILE}gcc"
fi

echo $PATH

make clean || exit
make || exit

if [ ! -e "bench_simd" ] ;then
	echo "oops, failed to build bench_simd"
	exit 1
fi

./bench.sh
