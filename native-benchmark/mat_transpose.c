#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

#include <benchmark.h>
#include <setup.h>
#include <utils.h>

#define ITEM_COUNT (16)

/******************************************************************************
 * Common code for vector transposeiplication tests
 *****************************************************************************/
typedef struct mtranspose_4x4_data {
	float32_t *input;
	float32_t *input_copy;
} mtranspose_4x4_data_t;

static void free_mtranspose_4x4_data_t(mtranspose_4x4_data_t *data) {
	if (!data) {
		return;
	}

	if (data->input) {
		free(data->input);
	}
	if (data->input_copy) {
		free(data->input_copy);
	}
	free(data);
}

static bool mtranspose_4x4_prepare(benchmark_t *benchmark) {
	assert(NULL != benchmark);
	mtranspose_4x4_data_t *data = NULL;

	data = zalloc(sizeof(mtranspose_4x4_data_t), 1);
	if (NULL == data) {
		goto fail;
	}
	memset(data, 0, sizeof(mtranspose_4x4_data_t));

	data->input = zalloc(sizeof(float32_t), ITEM_COUNT);
	data->input_copy = zalloc(sizeof(float32_t), ITEM_COUNT);

	if ((NULL == data->input)
		|| (NULL == data->input_copy))
	{
		goto fail;
	}

	for (size_t i = 0; i < ITEM_COUNT; i++) {
		data->input[i] = data->input_copy[i] = rand() % 1000;
	}

	benchmark->data = data;
	return true;

fail:
	free_mtranspose_4x4_data_t(data);
	return false;
}

static bool mtranspose_4x4_teardown(benchmark_t *benchmark) {
	assert(NULL != benchmark);
	mtranspose_4x4_data_t *data = benchmark->data;
	free_mtranspose_4x4_data_t(data);
	benchmark->data = NULL;
	return true;
}

static void print_matrix(float32_t *data) {
	for (size_t i = 0; i < 4; i++) {
		for (size_t j = 0; j < 4; j++) {
			printf("%4.4f ", data[4 * i + j]);
		}
		puts("");
	}
}

static bool mtranspose_4x4_verify(benchmark_t *benchmark) {
	assert(NULL != benchmark);
	mtranspose_4x4_data_t *data = (mtranspose_4x4_data_t*)benchmark->data;
	assert(NULL != data);
	assert(NULL != data->input);
	assert(NULL != data->input_copy);

	for (size_t i = 0; i < 4; i++) {
		for (size_t j = 0; j < i; j++) {
			if (data->input[j * 4 + i] != data->input_copy[i * 4 + j]) {
				puts("\n=========\n");
				print_matrix(data->input);
				puts("");
				print_matrix(data->input_copy);
				assert(0);
			}
		}
	}
	return true;
}

static bool mtranspose_4x4_clear_cache(benchmark_t *benchmark) {
	mtranspose_4x4_data_t *data = (mtranspose_4x4_data_t*)benchmark->data;
	__clear_cache(data->input, (data->input + ITEM_COUNT));
	__clear_cache(data->input_copy, (data->input_copy + ITEM_COUNT));
	return true;
}

/******************************************************************************
 * Linear implementation (vectorization is forcibly disabled)
 *****************************************************************************/
FORCE_NO_VEC_ATTRIBS
static void mat_transpose_linear(float32_t* __restrict a,
	float32_t* __restrict b)
{
	for (size_t i = 0; i < 4; i++) {
		for (size_t j = 0; j < i; j++) {
			float32_t tmp = a[4 * i + j];
			a[4 * i + j] = a[4 * j + i];
			a[4 * j + i] = tmp;
		}
	}
}

FORCE_NO_VEC_ATTRIBS
static bool mtranspose_4x4_action_linear(benchmark_t *benchmark) {
	mtranspose_4x4_data_t *data = (mtranspose_4x4_data_t*)benchmark->data;
	memcpy(data->input, data->input_copy, ITEM_COUNT * sizeof(float32_t));
	mat_transpose_linear(data->input, data->input_copy);
	return true;
}

benchmark_t bench_mat_transpose_linear = {
	.prepare = mtranspose_4x4_prepare,
	.teardown = mtranspose_4x4_teardown,
	.verify = mtranspose_4x4_verify,
	.clear_cache = mtranspose_4x4_clear_cache,
	.action = mtranspose_4x4_action_linear,
	.name = "Matrix Transposition 4x4 (linear/unvectorized)"
};

/******************************************************************************
 * Automatically vectorized implementation
 *****************************************************************************/
FORCE_VEC_ATTRIBS
static inline void mat_transpose_vectorized(float32_t* __restrict a,
	float32_t* __restrict b)
{
	for (size_t i = 0; i < 4; i++) {
		for (size_t j = 0; j < i; j++) {
			float32_t tmp = a[4 * i + j];
			a[4 * i + j] = a[4 * j + i];
			a[4 * j + i] = tmp;
		}
	}
}

FORCE_VEC_ATTRIBS
static bool mtranspose_4x4_action_vectorized(benchmark_t *benchmark) {
	mtranspose_4x4_data_t *data = (mtranspose_4x4_data_t*)benchmark->data;
	memcpy(data->input, data->input_copy, ITEM_COUNT * sizeof(float32_t));
	mat_transpose_vectorized(data->input, data->input_copy);
	return true;
}

benchmark_t bench_mat_transpose_vectorized = {
	.prepare = mtranspose_4x4_prepare,
	.teardown = mtranspose_4x4_teardown,
	.verify = mtranspose_4x4_verify,
	.clear_cache = mtranspose_4x4_clear_cache,
	.action = mtranspose_4x4_action_vectorized,
	.name = "Matrix Transposition 4x4 (vectorized)"
};

/******************************************************************************
 * Manually vectorized with NEON
 *****************************************************************************/
FORCE_VEC_ATTRIBS
static inline void mat_transpose_vectorized_manually(float32_t* __restrict a,
	float32_t* __restrict b)
{
	float32x4x4_t mtx = vld4q_f32(a);
	vst1q_f32(a, mtx.val[0]);
	vst1q_f32(a + 4, mtx.val[1]);
	vst1q_f32(a + 8, mtx.val[2]);
	vst1q_f32(a + 12, mtx.val[3]);

#if 0
	asm volatile(
		"vld4q.f32, {q15 - q0}, [%0]\n\t"
		"vst1q.f32 q0, [%0]!\n\t"
		"vst1q.f32 q1, [%0]!\n\t"
		"vst1q.f32 q2, [%0]!\n\t"
		"vst1q.f32 q3, [%0]!\n\t"
		:
		: "r"(a)
		: "memory",
			"q0", "q1", "q2", "q3",
			"q4", "q5", "q6", "q7",
			"q8", "q9", "q10", "q11",
			"q12", "q13", "q14", "q15"
	);
#endif
}

FORCE_VEC_ATTRIBS
static bool mtranspose_4x4_action_vectorized_manually(benchmark_t *benchmark) {
	mtranspose_4x4_data_t *data = (mtranspose_4x4_data_t*)benchmark->data;
	memcpy(data->input, data->input_copy, ITEM_COUNT * sizeof(float32_t));
	mat_transpose_vectorized_manually(data->input, data->input_copy);
	return true;
}

benchmark_t bench_mat_transpose_vectorized_manually = {
	.prepare = mtranspose_4x4_prepare,
	.teardown = mtranspose_4x4_teardown,
	.verify = mtranspose_4x4_verify,
	.clear_cache = mtranspose_4x4_clear_cache,
	.action = mtranspose_4x4_action_vectorized_manually,
	.name = "Matrix Transposition 4x4 (vectorized manually)"
};
