#include <benchmark.h>

#define RUN_BENCHMARK(name) do {\
	extern benchmark_t name;\
	run_benchmark(&name);\
} while (0);

int main(int argc, char **argv) {
	srand(0);

	measure_timer_resolution();

	RUN_BENCHMARK(bench_vec_mult_linear);
	RUN_BENCHMARK(bench_vec_mult_vectorized);
	RUN_BENCHMARK(bench_vec_mult_vectorized_preload);

	#if 0
	RUN_BENCHMARK(bench_mat_mult_linear);
	RUN_BENCHMARK(bench_mat_mult_vectorized);
	RUN_BENCHMARK(bench_mat_mult_vectorized_manually);

	RUN_BENCHMARK(bench_mat_transpose_linear);
	RUN_BENCHMARK(bench_mat_transpose_vectorized);
	RUN_BENCHMARK(bench_mat_transpose_vectorized_manually);
	#endif
	return 0;
}
