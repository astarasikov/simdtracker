#include <assert.h>
#include <time.h>
#include <math.h>
#include <stdio.h>

#include <benchmark.h>

#define PFSPEC "%4.4lf"

void print_stats(double *results, size_t count) {
	double min = 0;
	double max = 0;
	double sum = 0;
	size_t i = 0;

	for (i = 0; i < count; i++) {
		sum += results[i];
		min = fmin(min, results[i]);
		max = fmax(max, results[i]);
	}

	double mean = sum / count;
	double stddev = 0;

	for (i = 0; i < count; i++) {
		stddev += pow(results[i] - mean, 2.0);
	}
	stddev = sqrt(stddev / count);

	printf("range [" PFSPEC " ; " PFSPEC "]"
		" mean=" PFSPEC
		" stddev= " PFSPEC "\n", min, max, mean, stddev);
}

void run_benchmark(benchmark_t *benchmark) {
	int i = 0;
	double results[BENCH_RUNS] = {};
	assert(benchmark->action);
	assert(benchmark->clear_cache);
	assert(benchmark->verify);

	printf("Running \"%s\"\n", benchmark->name);

	if (benchmark->prepare) {
		assert(benchmark->prepare(benchmark));
	}
	assert(benchmark->clear_cache(benchmark));

	for (i = 0; i < BENCH_RUNS; i++) {
		double iteration_time = 0;
		struct timespec t_start = {}, t_end = {};

		assert(benchmark->clear_cache(benchmark));

		clock_gettime(CLOCK_MONOTONIC, &t_start);
		assert(benchmark->action(benchmark));
		clock_gettime(CLOCK_MONOTONIC, &t_end);

		assert(benchmark->verify(benchmark));

		iteration_time = ((t_end.tv_nsec - t_start.tv_nsec) / (1e6))
			+ ((t_end.tv_sec - t_start.tv_sec) * 1e3);
		results[i] = iteration_time;
		printf("[%d] -> %lf\n", i, results[i]);
	}
	print_stats(results, BENCH_RUNS);

	if (benchmark->teardown) {
		assert(benchmark->teardown(benchmark));
	}
}

void measure_timer_resolution(void) {
	struct timespec t_start = {}, t_end = {};
	clock_gettime(CLOCK_MONOTONIC, &t_start);
	while (1) {
		clock_gettime(CLOCK_MONOTONIC, &t_end);
		if ((t_end.tv_sec != t_start.tv_sec)
			|| (t_end.tv_nsec != t_start.tv_nsec))
		{
			break;
		}
	}
	double precision = ((t_end.tv_nsec - t_start.tv_nsec) / (1e6))
		+ ((t_end.tv_sec - t_start.tv_sec) * 1e3);
	printf("Timer resolution: %lf msec\n", precision);
}
