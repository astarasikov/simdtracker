#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

#include <benchmark.h>
#include <setup.h>
#include <utils.h>

#define ITEM_COUNT (16)

/******************************************************************************
 * Common code for vector multiplication tests
 *****************************************************************************/
typedef struct mmult_4x4_data {
	float32_t *input1;
	float32_t *input2;
	float32_t *output;
} mmult_4x4_data_t;

static void free_mmult_4x4_data_t(mmult_4x4_data_t *data) {
	if (!data) {
		return;
	}

	if (data->input1) {
		free(data->input1);
	}
	if (data->input2) {
		free(data->input2);
	}
	if (data->output) {
		free(data->output);
	}
	free(data);
}

static bool mmult_4x4_prepare(benchmark_t *benchmark) {
	assert(NULL != benchmark);
	mmult_4x4_data_t *data = NULL;
	
	data = zalloc(sizeof(mmult_4x4_data_t), 1);
	if (NULL == data) {
		goto fail;
	}
	memset(data, 0, sizeof(mmult_4x4_data_t));

	data->input1 = zalloc(sizeof(float32_t), ITEM_COUNT);
	data->input2 = zalloc(sizeof(float32_t), ITEM_COUNT);
	data->output = zalloc(sizeof(float32_t), ITEM_COUNT);

	if ((NULL == data->input1)
		|| (NULL == data->input2)
		|| (NULL == data->output))
	{
		goto fail;
	}

	for (size_t i = 0; i < ITEM_COUNT; i++) {
		data->input1[i] = rand() % 1000;
		data->input2[i] = rand() % 1000;
	}

	benchmark->data = data;
	return true;

fail:
	free_mmult_4x4_data_t(data);
	return false;
}

static bool mmult_4x4_teardown(benchmark_t *benchmark) {
	assert(NULL != benchmark);
	mmult_4x4_data_t *data = benchmark->data;
	free_mmult_4x4_data_t(data);
	benchmark->data = NULL;
	return true;
}

static bool mmult_4x4_verify(benchmark_t *benchmark) {
	assert(NULL != benchmark);
	mmult_4x4_data_t *data = (mmult_4x4_data_t*)benchmark->data;
	assert(NULL != data);
	assert(NULL != data->input1);
	assert(NULL != data->input2);
	assert(NULL != data->output);

	for (size_t i = 0; i < 4; i++) {
		for (size_t j = 0; j < 4; j++) {
			float val = 0.0f;
			for (size_t k = 0; k < 4; k++) {
				val += data->input1[4 * k + i] * data->input2[4 * j + k];
			}
			if (val != data->output[4 * j + i]) {
				return false;
			}
		}
	}

	return true;
}

static bool mmult_4x4_clear_cache(benchmark_t *benchmark) {
	mmult_4x4_data_t *data = (mmult_4x4_data_t*)benchmark->data;
	__clear_cache(data->input1, (data->input1 + ITEM_COUNT));
	__clear_cache(data->input2, (data->input2 + ITEM_COUNT));
	__clear_cache(data->output, (data->output + ITEM_COUNT));
	return true;
}

/******************************************************************************
 * Linear implementation (vectorization is forcibly disabled)
 *****************************************************************************/
FORCE_NO_VEC_ATTRIBS
static void mat_mult_linear(float32_t* __restrict a,
	float32_t* __restrict b,
	float32_t* __restrict out)
{
	for (size_t i = 0; i < 4; i++) {
		for (size_t j = 0; j < 4; j++) {
			out[4 * j + i] = 0.0f;
			for (size_t k = 0; k < 4; k++) {
				out[4 * j + i] += a[4 * k + i] * b[4 * j + k];
			}
		}
	}
}

FORCE_NO_VEC_ATTRIBS
static bool mmult_4x4_action_linear(benchmark_t *benchmark) {
	mmult_4x4_data_t *data = (mmult_4x4_data_t*)benchmark->data;
	mat_mult_linear(data->input1, data->input2, data->output);
	return true;
}

benchmark_t bench_mat_mult_linear = {
	.prepare = mmult_4x4_prepare,
	.teardown = mmult_4x4_teardown,
	.verify = mmult_4x4_verify,
	.clear_cache = mmult_4x4_clear_cache,
	.action = mmult_4x4_action_linear,
	.name = "Matrix Multiplication 4x4 (linear/unvectorized)"
};

/******************************************************************************
 * Automatically vectorized implementation
 *****************************************************************************/
FORCE_VEC_ATTRIBS
static inline void mat_mult_vectorized(float32_t* __restrict a,
	float32_t* __restrict b,
	float32_t* __restrict out)
{
	for (size_t i = 0; i < 4; i++) {
		for (size_t j = 0; j < 4; j++) {
			out[4 * j + i] = 0.0f;
			for (size_t k = 0; k < 4; k++) {
				out[4 * j + i] += a[4 * k + i] * b[4 * j + k];
			}
		}
	}
}

FORCE_VEC_ATTRIBS
static bool mmult_4x4_action_vectorized(benchmark_t *benchmark) {
	mmult_4x4_data_t *data = (mmult_4x4_data_t*)benchmark->data;
	mat_mult_vectorized(data->input1, data->input2, data->output);
	return true;
}

benchmark_t bench_mat_mult_vectorized = {
	.prepare = mmult_4x4_prepare,
	.teardown = mmult_4x4_teardown,
	.verify = mmult_4x4_verify,
	.clear_cache = mmult_4x4_clear_cache,
	.action = mmult_4x4_action_vectorized,
	.name = "Matrix Multiplication 4x4 (vectorized)"
};

/******************************************************************************
 * Manually vectorized with NEON
 *****************************************************************************/
FORCE_VEC_ATTRIBS
static inline void mat_mult_vectorized_manually(float32_t* __restrict a,
	float32_t* __restrict b,
	float32_t* __restrict out)
{
	asm volatile(
		"vld1.32 {d16 - d19}, [%0]!\n\t"
		"vld1.32 {d20 - d23}, [%0]\n\t"
		"vld1.32 {d0 - d3}, [%1]!\n\t"
		"vld1.32 {d4 - d7}, [%1]\n\t"
	
		"vmul.f32 q12, q8, d0[0]\n\t"
		"vmul.f32 q13, q8, d2[0]\n\t"
		"vmul.f32 q14, q8, d4[0]\n\t"
		"vmul.f32 q15, q8, d6[0]\n\t"

		"vmla.f32 q12, q9, d0[1]\n\t"
		"vmla.f32 q13, q9, d2[1]\n\t"
		"vmla.f32 q14, q9, d4[1]\n\t"
		"vmla.f32 q15, q9, d6[1]\n\t"

		"vmla.f32 q12, q10, d1[0]\n\t"
		"vmla.f32 q13, q10, d3[0]\n\t"
		"vmla.f32 q14, q10, d5[0]\n\t"
		"vmla.f32 q15, q10, d7[0]\n\t"

		"vmla.f32 q12, q11, d1[1]\n\t"
		"vmla.f32 q13, q11, d3[1]\n\t"
		"vmla.f32 q14, q11, d5[1]\n\t"
		"vmla.f32 q15, q11, d7[1]\n\t"

		"vst1.32 {d24-d27}, [%2]!\n\t"
		"vst1.32 {d28-d31}, [%2]\n\t"
		:
		: "r"(a), "r"(b), "r"(out)
		: "memory",
			"q0", "q1", "q2", "q3",
			"q4", "q5", "q6", "q7",
			"q8", "q9", "q10", "q11",
			"q12", "q13", "q14", "q15"
	);
}

FORCE_VEC_ATTRIBS
static bool mmult_4x4_action_vectorized_manually(benchmark_t *benchmark) {
	mmult_4x4_data_t *data = (mmult_4x4_data_t*)benchmark->data;
	mat_mult_vectorized_manually(data->input1, data->input2, data->output);
	return true;
}

benchmark_t bench_mat_mult_vectorized_manually = {
	.prepare = mmult_4x4_prepare,
	.teardown = mmult_4x4_teardown,
	.verify = mmult_4x4_verify,
	.clear_cache = mmult_4x4_clear_cache,
	.action = mmult_4x4_action_vectorized_manually,
	.name = "Matrix Multiplication 4x4 (vectorized manually)"
};
