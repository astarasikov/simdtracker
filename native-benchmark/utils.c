#include <utils.h>

void *
zalloc(size_t size, size_t count)
{
	void *ret = NULL;
	ret = malloc(count * size);
	if (NULL == ret) {
		return NULL;
	}
	memset(ret, 0, count * size);
	return ret;
}
