#!/bin/bash

function bench_at_freq {
	adb shell su -c "echo performance > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"
	adb shell su -c "echo $1 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"
	adb shell su -c "echo $1 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"
	adb shell su -c "echo $1 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed"
	adb shell su -c "/data/local/bench_simd > /data/local/bench_$1.txt"
	adb shell su -c "chmod 777 /data/local/bench_$1.txt"
	mkdir -p results
	adb pull /data/local/bench_$1.txt results/
}

adb push ./bench_simd /sdcard/
adb shell su -c "cp /sdcard/bench_simd /data/local/"
adb shell su -c "chmod 777 /data/local/bench_simd"

bench_at_freq 2265600
bench_at_freq 883200
