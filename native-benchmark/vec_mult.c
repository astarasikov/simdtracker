#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

#include <benchmark.h>
#include <setup.h>
#include <utils.h>

#define ITEM_COUNT (100)
#define PLD_SIZE (1 << 4)

/******************************************************************************
 * Common code for vector multiplication tests
 *****************************************************************************/
typedef struct vmult_data {
	float32_t *input1;
	float32_t *input2;
	float32_t *output;
} vmult_data_t;

static void free_vmult_data_t(vmult_data_t *data) {
	if (!data) {
		return;
	}

	if (data->input1) {
		free(data->input1);
	}
	if (data->input2) {
		free(data->input2);
	}
	if (data->output) {
		free(data->output);
	}
	free(data);
}

static bool vmult_prepare(benchmark_t *benchmark) {
	assert(NULL != benchmark);
	vmult_data_t *data = NULL;
	
	data = zalloc(sizeof(vmult_data_t), 1);
	if (NULL == data) {
		goto fail;
	}
	memset(data, 0, sizeof(vmult_data_t));

	data->input1 = zalloc(sizeof(float32_t), ITEM_COUNT);
	data->input2 = zalloc(sizeof(float32_t), ITEM_COUNT);
	data->output = zalloc(sizeof(float32_t), ITEM_COUNT);

	if ((NULL == data->input1)
		|| (NULL == data->input2)
		|| (NULL == data->output))
	{
		goto fail;
	}

	for (size_t i = 0; i < ITEM_COUNT; i++) {
		data->input1[i] = rand() % 10000;
		data->input2[i] = rand() % 10000;
	}

	benchmark->data = data;
	return true;

fail:
	free_vmult_data_t(data);
	return false;
}

static bool vmult_teardown(benchmark_t *benchmark) {
	assert(NULL != benchmark);
	vmult_data_t *data = benchmark->data;
	free_vmult_data_t(data);
	benchmark->data = NULL;
	return true;
}

static bool vmult_verify(benchmark_t *benchmark) {
	assert(NULL != benchmark);
	int i = 0;
	vmult_data_t *data = (vmult_data_t*)benchmark->data;
	assert(NULL != data);
	assert(NULL != data->input1);
	assert(NULL != data->input2);
	assert(NULL != data->output);

	for (i = 0; i < ITEM_COUNT; i++) {
		if (data->output[i] != data->input1[i] * data->input2[i]) {
			return false;
		}
	}
	return true;
}

static bool vmult_clear_cache(benchmark_t *benchmark) {
	vmult_data_t *data = (vmult_data_t*)benchmark->data;
	__clear_cache(data->input1, (data->input1 + ITEM_COUNT));
	__clear_cache(data->input2, (data->input2 + ITEM_COUNT));
	__clear_cache(data->output, (data->output + ITEM_COUNT));
	return true;
}

/******************************************************************************
 * Linear implementation (vectorization is forcibly disabled)
 *****************************************************************************/
FORCE_NO_VEC_ATTRIBS
static void vec_mult_linear(float32_t* __restrict a,
	float32_t* __restrict b,
	float32_t* __restrict out)
{
	for (size_t i = 0; i < ITEM_COUNT; i++) {
		out[i] = a[i] * b[i];
	}
}

FORCE_NO_VEC_ATTRIBS
static bool vmult_action_linear(benchmark_t *benchmark) {
	vmult_data_t *data = (vmult_data_t*)benchmark->data;
	vec_mult_linear(data->input1, data->input2, data->output);
	return true;
}

benchmark_t bench_vec_mult_linear = {
	.prepare = vmult_prepare,
	.teardown = vmult_teardown,
	.verify = vmult_verify,
	.clear_cache = vmult_clear_cache,
	.action = vmult_action_linear,
	.name = "Vector Multiplication (linear/unvectorized)"
};

/******************************************************************************
 * Automatically vectorized implementation
 *****************************************************************************/
__attribute__((optimize("tree-vectorize")))
__attribute__((optimize("tree-slp-vectorize")))
__attribute__((optimize("fast-math")))
__attribute__((optimize("unsafe-math-optimizations")))
static inline void vec_mult_vectorized(float32_t* __restrict a,
	float32_t* __restrict b,
	float32_t* __restrict out)
{
	for (size_t i = 0; i < ITEM_COUNT; i++) {
		out[i] = a[i] * b[i];
	}
}

FORCE_VEC_ATTRIBS
static bool vmult_action_vectorized(benchmark_t *benchmark) {
	vmult_data_t *data = (vmult_data_t*)benchmark->data;
	vec_mult_vectorized(data->input1, data->input2, data->output);
	return true;
}

benchmark_t bench_vec_mult_vectorized = {
	.prepare = vmult_prepare,
	.teardown = vmult_teardown,
	.verify = vmult_verify,
	.clear_cache = vmult_clear_cache,
	.action = vmult_action_vectorized,
	.name = "Vector Multiplication (vectorized)"
};

/******************************************************************************
 * Automatically vectorized implementation with PLD (preload) instruction
 *****************************************************************************/
FORCE_VEC_ATTRIBS
static inline void vec_mult_vectorized_pld(float32_t* __restrict a,
	float32_t* __restrict b,
	float32_t* __restrict out)
{
	for (size_t i = 0; i < ITEM_COUNT; i++) {
		if (i % PLD_SIZE == 0) {
			__builtin_prefetch (&a[i + PLD_SIZE], 0, 0);
			__builtin_prefetch (&b[i + PLD_SIZE], 0, 0);
			__builtin_prefetch (&out[i + PLD_SIZE], 1, 0);
		}
		out[i] = a[i] * b[i];
	}
}

FORCE_VEC_ATTRIBS
static bool vmult_action_vectorized_pld(benchmark_t *benchmark) {
	vmult_data_t *data = (vmult_data_t*)benchmark->data;
	vec_mult_vectorized_pld(data->input1, data->input2, data->output);
	return true;
}

benchmark_t bench_vec_mult_vectorized_preload = {
	.prepare = vmult_prepare,
	.teardown = vmult_teardown,
	.verify = vmult_verify,
	.clear_cache = vmult_clear_cache,
	.action = vmult_action_vectorized_pld,
	.name = "Vector Multiplication (vectorized with PLD)"
};
