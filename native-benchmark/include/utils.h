#ifndef __UTILS_H__
#define __UTILS_H__

#include <stdint.h>
#include <stdlib.h>

extern void *zalloc(size_t size, size_t count);

#endif //__UTILS_H__
