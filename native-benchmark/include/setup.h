#ifndef __SETUP_H__
#define __SETUP_H__

#ifdef __ARM_NEON__
#include <arm_neon.h>
#else
typedef float float32_t;
#endif

#define FORCE_VEC_ATTRIBS \
	__attribute__((optimize("tree-vectorize"))) \
	__attribute__((optimize("fast-math"))) \
	__attribute__((optimize("unsafe-math-optimizations")))
	
#define FORCE_NO_VEC_ATTRIBS \
	__attribute__((optimize("no-tree-vectorize"))) \
	__attribute__((optimize("no-tree-slp-vectorize"))) \
	__attribute__((optimize("no-fast-math"))) \
	__attribute__((optimize("no-unsafe-math-optimizations"))) \
	__attribute__((optimize("O0"))) \
	__attribute__((noinline))


#endif //__SETUP_H__
