#ifndef __BENCHMARK_H__
#define __BENCHMARK_H__

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#define BENCH_RUNS 50

struct benchmark;
typedef bool (benchmark_cb_t)(struct benchmark *);

typedef struct benchmark {
	benchmark_cb_t *action;
	void *data;
	const char *name;

	benchmark_cb_t *clear_cache;
	benchmark_cb_t *verify;
	
	benchmark_cb_t *prepare;
	benchmark_cb_t *teardown;
} benchmark_t;

void print_stats(double *results, size_t count);
void run_benchmark(benchmark_t *benchmark);
void measure_timer_resolution(void);

#endif //__BENCHMARK_H__
