package io.github.astarasikov.neontracker;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.renderscript.Allocation;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicConvolve3x3;
import android.renderscript.ScriptIntrinsicConvolve5x5;
import android.util.Log;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.Size;

public class GaussConvolution {
    Context mContext;

    protected Bitmap mInputBitmap;
    protected Bitmap mOutputBitmap;
    RenderScript mRenderScript;
    Allocation mInputAllocation;
    Allocation mOutputAllocation;
    ScriptIntrinsicConvolve5x5 mConvo5x5;

    GaussConvolution(Context context) {
        mContext = context;
    }

    public void release() {
        if (null != mConvo5x5) {
            mConvo5x5.destroy();
        }

        if (null != mRenderScript) {
            mRenderScript.destroy();
        }

        if (null != mInputBitmap) {
            mInputBitmap.recycle();
        }

        if (null != mOutputBitmap) {
            mOutputBitmap.recycle();
        }
    }


    public synchronized boolean convolve(Mat in, Mat out) {
        int in_width = in.width();
        int in_height = in.height();
        int out_width = out.width();
        int out_height = out.height();

        int in_dims = in.dims();
        int out_dims = out.dims();

        if ((0 == in_dims) || (0 == out_dims)) {
            return false;
        }

        if ((0 == in_width) || (0 == in_height) || (0 == out_width) || (0 == out_height)) {
            return false;
        }

        if ((null == mInputBitmap) ||
                (null == mOutputBitmap) ||
                (null == mRenderScript) ||
                (mInputBitmap.getWidth() != in_width) ||
                (mInputBitmap.getHeight() != in_height) ||
                (mOutputBitmap.getWidth() != out_width) ||
                (mOutputBitmap.getHeight() != out_height))
        {
            release();

            mInputBitmap = Bitmap.createBitmap(in_width, in_height, Bitmap.Config.ARGB_8888);
            mOutputBitmap = Bitmap.createBitmap(out_width, out_height, Bitmap.Config.ARGB_8888);
            mRenderScript = mRenderScript.create(mContext);
            mInputAllocation = Allocation.createFromBitmap(mRenderScript, mInputBitmap,
                    Allocation.MipmapControl.MIPMAP_NONE, Allocation.USAGE_SCRIPT);
            mOutputAllocation = Allocation.createTyped(mRenderScript, mInputAllocation.getType());
            mConvo5x5 = ScriptIntrinsicConvolve5x5.create(mRenderScript, mInputAllocation.getElement());
        }

        try {
            Log.i("GAUSSSSSS", String.format("in <%d %d>, inbmp=<%d %d>", in_width, in_height,
                    mInputBitmap.getWidth(), mInputBitmap.getHeight()));

            Utils.matToBitmap(in, mInputBitmap);
            mConvo5x5.setInput(mInputAllocation);
            mConvo5x5.forEach(mOutputAllocation);
            mOutputAllocation.copyTo(mOutputBitmap);
            Utils.bitmapToMat(mOutputBitmap, out);
            return true;
        }
        catch (Throwable t) {
            Log.i("GAUSSS", t.getMessage());
            return false;
        }

    }
}
