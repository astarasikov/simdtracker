package io.github.astarasikov.neontracker;

import android.graphics.Bitmap;
import org.opencv.android.*;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

import java.lang.reflect.Field;

public class NEONTrackerActivity
	extends Activity
	implements CvCameraViewListener2
{
    private static final String    TAG = "NEONTrackerActivity";

    private Mat mMarker;
	private Mat mImage;
	private Mat mOutput;
	private int frameNo = 0;
    private GaussConvolution mConvolution;

    private CameraBridgeViewBase   mOpenCvCameraView;

    private BaseLoaderCallback  mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                    System.loadLibrary("neon_tracker");
                    mOpenCvCameraView.enableView();
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    public NEONTrackerActivity() {
        Log.i(TAG, "Instantiated new " + this.getClass());
    }

	protected volatile boolean holdingResources = false;

	protected synchronized void initResources() {
		holdingResources = true;
		Init();
	}

	protected void releaseImage() {
		if (null != mImage) {
			mImage.release();
			mImage = null;
		}
	}

	protected synchronized void releaseResources() {
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
		frameNo = 0;

		Release();
		releaseImage();

		if (null != mMarker) {
			mMarker.release();
			mMarker = null;
		}

		if (null != mOutput) {
			mOutput.release();
			mOutput = null;
		}

        if (null != mConvolution) {
            mConvolution.release();
            mConvolution = null;
        }

		holdingResources = false;
	}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.neontracker_surface_view);

        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.neontracker_activity_surface_view);
		mOpenCvCameraView.setMaxFrameSize(640, 480);
        mOpenCvCameraView.setCvCameraViewListener(this);
    }

    @Override
    public void onPause()
    {
        super.onPause();
		releaseResources();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, this, mLoaderCallback);
    }

    public void onDestroy() {
        super.onDestroy();
		releaseResources();
    }

    public void onCameraViewStarted(int width, int height) {
		initResources();
    }

    public void onCameraViewStopped() {
		releaseResources();
    }

    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
		long tStart = System.currentTimeMillis();

		initResources();
		releaseImage();
		frameNo++;

		if (frameNo < 10) {
			return inputFrame.gray();
		}

		if (null == mMarker) {
			mMarker = inputFrame.gray();
			if (null != mMarker) {
				SetMarker(mMarker.getNativeObjAddr());
			}
			return inputFrame.gray();
		}

		if (null == mOutput) {
			mOutput = inputFrame.gray();
		}

		if (null == mImage) {
			mImage = inputFrame.gray();
		}

		if ((null == mImage) || (null == mOutput) || (null == mMarker)) {
			return inputFrame.rgba();
		}

		Log.i(TAG, String.format("Marker <%d %d>, Image <%d %d>",
			mMarker.width(), mMarker.height(),
			mImage.width(), mImage.height()));

		FindFeatures(mImage.getNativeObjAddr(),
			mOutput.getNativeObjAddr());

		long tEnd = System.currentTimeMillis();
		long tProc = tEnd - tStart;
		Log.i(TAG, String.format("Processed in %d, %f FPS",
			tProc, 1000.0 / tProc));

        return mOutput;
    }

    protected Mat gInMat = new Mat(0xdeadbeef);
    protected Mat gOutMat = new Mat(0xdeadbeef);

    protected boolean setMatAddr(Mat mat, long addr) {
        try {
            Field f = Mat.class.getDeclaredField("nativeObj");
            f.setAccessible(true);
            f.set(mat, addr);
            return true;
        }
        catch (Throwable t) {
            Log.i(TAG, t.getMessage());
            return false;
        }
    }

    public boolean gaussConvolve(long matInAddr, long matOutAddr) {
        if (null == mConvolution) {
            mConvolution = new GaussConvolution(this);
        }

        if (!setMatAddr(gInMat, matInAddr) || !setMatAddr(gOutMat, matOutAddr)) {
            return false;
        }

        if (null != mConvolution) {
            return mConvolution.convolve(gInMat, gOutMat);
        }
        return false;
    }

	public native void Release();
	public native void Init();
	public native void SetMarker(long addrMarker);
	public native void FindFeatures(long addrImage, long addrOut);
}
