LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

include ../../sdk/native/jni/OpenCV.mk

LOCAL_CPPFLAGS := -frtti -fexceptions
LOCAL_MODULE    := neon_tracker
LOCAL_SRC_FILES := jni_part.cpp opencv_rs/rs_orb.cpp
LOCAL_LDLIBS +=  -llog -ldl

include $(BUILD_SHARED_LIBRARY)
