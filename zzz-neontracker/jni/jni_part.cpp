#include <jni.h>
#include <android/log.h>

#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "opencv_rs/rs_orb.hpp"

#if 1
	#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG, \
		"NeonTrackerJNI", __VA_ARGS__)
#else
	#define LOGD(...) do {} while (0)
#endif

enum {
	USE_KNN = 1,
	KNN_COUNT = 3,
	MIN_MATCHES = 10,

	NUM_FEATURES = 200,

	USE_RS = 0,
};

#if (USE_RS == 0)
	#define T_DETECTOR cv::OrbFeatureDetector(NUM_FEATURES)
	#define T_EXTRACTOR cv::ORB(50)
#else
	#define T_DETECTOR cv::RsOrbFeatureDetector(NUM_FEATURES)
	#define T_EXTRACTOR cv::RsORB()
#endif

typedef std::vector<cv::DMatch> match_vec;
typedef std::vector<cv::KeyPoint> keypoint_vec;
typedef std::vector<cv::Point2f> point_vec;

struct Tracker {
	cv::FeatureDetector *detector;
	cv::DescriptorExtractor *extractor;
	cv::DescriptorMatcher *matcher;

	const cv::Mat *image_marker;
	keypoint_vec *keypoint_marker;
	cv::Mat *descriptors_marker;
	point_vec marker_corners;
	cv::Point2f marker_origin;

	Tracker() :
		detector(new T_DETECTOR),
		extractor(new T_EXTRACTOR),
		matcher(new cv::BFMatcher(cv::NORM_HAMMING)),
		image_marker(NULL),
		keypoint_marker(NULL),
		descriptors_marker(NULL),
		marker_corners(point_vec(4)),
		marker_origin(cv::Point2f())
	{
	}

	void freeMarkerResources(void)
	{
		if (keypoint_marker) {
			delete keypoint_marker;
			keypoint_marker = NULL;
		}
		if (descriptors_marker) {
			delete descriptors_marker;
			descriptors_marker = NULL;
		}
	}

	~Tracker() {
		delete detector;
		delete extractor;
	}

	//XXX: let upper level handle this
	void drawFrame(const cv::Mat &scene, const cv::Mat &homography,
		cv::Mat& out)
	{
		point_vec scene_corners(4);
		cv::perspectiveTransform(marker_corners, scene_corners, homography);

		for (int i = 0; i < 4; i++) {
			line(out, marker_corners[i], marker_corners[(i + 1) % 4],
				cv::Scalar(255, 0, 0), 4);

			line(out, scene_corners[i] + marker_origin,
				scene_corners[(i + 1) % 4] + marker_origin,
				cv::Scalar(0, 255, 0), 4);
		}
	}

	void filterMatches(const match_vec &in, match_vec &out, float min_dist)
	{
		for (match_vec::const_iterator it = in.begin(); it != in.end(); it++) {
			const cv::DMatch &match = *it;
			if (match.distance < fmax(3 * min_dist, 0.02f)) {
				out.push_back(match);
			}
		}
	}

	void matchSimple(const cv::Mat &dsc_marker, const cv::Mat &dsc_scene,
		match_vec &out)
	{
		match_vec matches;
		matcher->match(dsc_marker, dsc_scene, matches);

		float min_dist = 999.0f;
		for (match_vec::iterator it = matches.begin(); it != matches.end(); it++) {
			const cv::DMatch &match = *it;
			min_dist = std::min(min_dist, match.distance);
		}
		LOGD("Min distance %4.4f", min_dist);

		filterMatches(matches, out, min_dist);
	}

	void matchKnn(const cv::Mat &dsc_marker, const cv::Mat &dsc_scene,
		match_vec &out)
	{
		std::vector<match_vec> mvec;
		matcher->knnMatch(dsc_marker, dsc_scene, mvec, KNN_COUNT);
		LOGD("KNN Matches: %d", mvec.size());
		for (size_t i = 0; i < mvec.size(); i++) {
			if (mvec[i][0].distance > 0.8 * (mvec[i][1].distance)) {
				continue;
			}
			if (mvec[i].size() != KNN_COUNT) {
				continue;
			}
			out.push_back(mvec[i][0]);
		}
	}

	void showMatches(const cv::Mat &img_scene, cv::Mat &img_matches) {
		using namespace cv;

		if ((NULL == image_marker) ||
			(NULL == keypoint_marker) ||
			(NULL == descriptors_marker))
		{
			return;
		}

		keypoint_vec kp_scene, kp_marker = *keypoint_marker;
		detector->detect(img_scene, kp_scene);

		Mat dsc_scene, dsc_marker = *descriptors_marker;
		extractor->compute(img_scene, kp_scene, dsc_scene);

		BFMatcher matcher(NORM_HAMMING);
		match_vec good_matches;

		if (USE_KNN) {
			matchKnn(dsc_marker, dsc_scene, good_matches);
		}
		else {
			matchSimple(dsc_marker, dsc_scene, good_matches);
		}

		LOGD("Matched %d keypoints", good_matches.size());

		std::vector<Point2f> pt_marker, pt_scene;
		for (match_vec::iterator it = good_matches.begin();
			it != good_matches.end(); it++)
		{
			cv::DMatch &match = *it;
			pt_marker.push_back(kp_marker[match.queryIdx].pt);
			pt_scene.push_back(kp_scene[match.trainIdx].pt);
		}

		drawMatches(*image_marker, kp_marker, img_scene, kp_scene,
			good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
			std::vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

		if (min(pt_marker.size(), pt_scene.size()) > min(4, (int)MIN_MATCHES)) {
			Mat H = findHomography(pt_marker, pt_scene, LMEDS);
			drawFrame(img_scene, H, img_matches);
		}

		//XXX: dirty workaround for android
		if (img_matches.size() != img_scene.size()) {
			cv::resize(img_matches, img_matches, img_scene.size());
		}
	}

	void setMarkerImage(const cv::Mat *img_marker)
	{
		freeMarkerResources();
		keypoint_marker = new keypoint_vec();
		descriptors_marker = new cv::Mat();
		image_marker = img_marker;

		detector->detect(*img_marker, *keypoint_marker);
		extractor->compute(*img_marker, *keypoint_marker, *descriptors_marker);

		marker_corners[0] = cv::Point(0, 0);
		marker_corners[1] = cv::Point(img_marker->cols, 0);
		marker_corners[2] = cv::Point(img_marker->cols, img_marker->rows);
		marker_corners[3] = cv::Point(0, img_marker->rows);
		marker_origin = cv::Point2f(img_marker->cols, 0);
		//XXX: allow user to specify
	}
};

static Tracker *gTracker = NULL;
static JNIEnv *jenv = NULL;
static jobject jobj;

extern "C" {
	int tracker_gconvolve(void *mat_input, void *mat_output) {
		if ((NULL == mat_input)
			|| (NULL == mat_output)
			|| (NULL == jenv))
		{
			return 0;
		}

		LOGD(">> env=%lx", (long)jenv);
		jclass cls = jenv->FindClass("io/github/astarasikov/neontracker/NEONTrackerActivity");
		LOGD(">> cls=%lx", (long)cls);
		jmethodID convmethod = jenv->GetMethodID(cls, "gaussConvolve", "(JJ)Z");
		LOGD(">> meth=%lx", (long)convmethod);
		return jenv->CallBooleanMethod(jobj, convmethod, (jlong)mat_input, (jlong)mat_output);
	}

	JNIEXPORT void JNICALL
		Java_io_github_astarasikov_neontracker_NEONTrackerActivity_Init
		(JNIEnv* env, jobject object)
	{
		jenv = env;
		jobj = (jobject)env->NewGlobalRef(object);
		if (NULL != gTracker) {
			return;
		}
		gTracker = new Tracker();
	}

	JNIEXPORT void JNICALL
		Java_io_github_astarasikov_neontracker_NEONTrackerActivity_Release
		(JNIEnv*, jobject)
	{
		if (NULL == gTracker) {
			return;
		}

		delete gTracker;
		gTracker = NULL;

		if (NULL != jenv) {
			jenv->DeleteGlobalRef(jobj);
			jenv = NULL;
		}
	}

	JNIEXPORT void JNICALL
		Java_io_github_astarasikov_neontracker_NEONTrackerActivity_SetMarker
		(JNIEnv*, jobject, jlong marker)
	{
		if (NULL == gTracker) {
			return;
		}
		gTracker->setMarkerImage((cv::Mat*)marker);
	}

	JNIEXPORT void JNICALL
		Java_io_github_astarasikov_neontracker_NEONTrackerActivity_FindFeatures
		(JNIEnv*, jobject, jlong image, jlong out)
	{
		cv::Mat& mImage  = *(cv::Mat*)image;
		cv::Mat& mOut  = *(cv::Mat*)out;

		if (NULL == gTracker) {
			return;
		}

		gTracker->showMatches(mImage, mOut);
	}
}
