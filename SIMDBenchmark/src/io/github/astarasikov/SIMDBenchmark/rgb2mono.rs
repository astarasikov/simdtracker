#pragma version(1)
#pragma rs java_package_name(io.github.astarasikov.SIMDBenchmark)
#pragma rs_fp_relaxed

const static float3 grayCoeff = { 0.3, 0.5, 0.1 };

void root(const uchar4 *in, uchar4 *out, const void *userData,
    uint32_t x, uint32_t y)
{
    float4 f4 = rsUnpackColor8888(*in);
    float3 mono = dot(f4.rgb, grayCoeff);
    *out = rsPackColorTo8888(mono);
}