package io.github.astarasikov.SIMDBenchmark;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.renderscript.Allocation;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicConvolve3x3;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class BenchmarkActivity extends Activity {
    final static String LOG_TAG = "SIMDBenchmark";

    final int mImageWidth = 640;
    final int mImageHeight = 480;

    Bitmap mSourceBitmap;
    Bitmap mOutputBitmap;
    RenderScript mRenderScript;
    ScriptC_rgb2mono mScriptRgb2Mono;
    Allocation mInputAllocation;
    Allocation mOutputAllocation;

    ImageView mInputView;
    ImageView mOutputView;

    synchronized void preloadBitmaps() {
        if (null == mSourceBitmap) {
            Bitmap tmp = BitmapFactory.decodeResource(
                    BenchmarkActivity.this.getResources(), R.drawable.house);
            mSourceBitmap = Bitmap.createScaledBitmap(tmp,
                    mImageWidth, mImageHeight, false);
            mInputView.setImageBitmap(mSourceBitmap);
        }
        if (null == mOutputBitmap) {
            mOutputBitmap = Bitmap.createBitmap(mSourceBitmap);
        }
    }

    ScriptIntrinsicConvolve3x3 mConvolution;

    synchronized void initRs() {
        if (null == mRenderScript) {
            mRenderScript = RenderScript.create(this);
            mInputAllocation = Allocation.createFromBitmap(mRenderScript,
                    mSourceBitmap, Allocation.MipmapControl.MIPMAP_NONE,
                    Allocation.USAGE_SCRIPT);
            mOutputAllocation = Allocation.createTyped(mRenderScript,
                    mInputAllocation.getType());
        }
    }

    void processConvolutionRs() {
        initRs();

        if (mConvolution == null) {
            mConvolution = ScriptIntrinsicConvolve3x3.create(mRenderScript,
                    mInputAllocation.getElement());
            mConvolution.setCoefficients(new float[]{
                    -1, -2, -1,
                    0, 1, 0,
                    1, 2, 1,
            });
        }
        mConvolution.setInput(mInputAllocation);
        mConvolution.forEach(mOutputAllocation);
        mOutputAllocation.copyTo(mOutputBitmap);
        mOutputView.setImageBitmap(mOutputBitmap);
    }

    void processGrayscaleRs() {
        initRs();

        if (null == mScriptRgb2Mono) {
            mScriptRgb2Mono = new ScriptC_rgb2mono(mRenderScript,
                    getResources(), R.raw.rgb2mono);
        }
        mScriptRgb2Mono.forEach_root(mInputAllocation, mOutputAllocation);
        mOutputAllocation.copyTo(mOutputBitmap);
        mOutputView.setImageBitmap(mOutputBitmap);
    }

    synchronized void processRsConvo() {
        long t_start = System.currentTimeMillis();
        processConvolutionRs();
        long t_end = System.currentTimeMillis();

        long dt = t_end - t_start;
        Log.i(LOG_TAG, String.format("Convolution processed in %d, %f FPS", dt, 1000.0 / dt));
    }

    synchronized void processRsGrayscale() {
        long t_start = System.currentTimeMillis();
        processGrayscaleRs();
        long t_end = System.currentTimeMillis();

        long dt = t_end - t_start;
        Log.i(LOG_TAG, String.format("Grayscale processed in %d, %f FPS", dt, 1000.0 / dt));
    }

    Button.OnClickListener mButtonListener = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            preloadBitmaps();
            switch (view.getId()) {
                case R.id.button_convo:
                    processRsConvo();
                    break;
                case R.id.button_gray:
                    processRsGrayscale();
                    break;
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mInputView = (ImageView)findViewById(R.id.image_input);
        mOutputView = (ImageView)findViewById(R.id.image_output);
        findViewById(R.id.button_convo).setOnClickListener(mButtonListener);
        findViewById(R.id.button_gray).setOnClickListener(mButtonListener);
    }
}
